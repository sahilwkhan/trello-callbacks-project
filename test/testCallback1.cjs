/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');

const getBoardInformation = require("../callback1.cjs");

const boardsJSONFilePath = "./boards.json";

// Read boards.json and Convert it into Javascript object

fs.readFile(boardsJSONFilePath, (error1, boardData) => {
	if (error1) {
		console.error(error1);
	}
	else {
		let boardDataArray = JSON.parse(boardData);
		
		getBoardInformation((error2, boardInformation) => {
			if (error2) {
				console.error(error2);
				console.log("Returned to callback function.")
			}
			else {
				console.log("Board ID found and ID information is returned successfully inside callback function :");
				console.log(boardInformation);
			}
		}, boardDataArray[0].id);

	}
});



