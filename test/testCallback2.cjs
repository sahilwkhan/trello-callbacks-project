/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');

const getAllListsFromBoardInformation = require("../callback2.cjs");

const boardsJSONFilePath = "./boards.json";

// 
fs.readFile(boardsJSONFilePath, (error1, boardData) => {
	if (error1) {
		console.error(error1);
	}
	else {
		let boardDataArray = JSON.parse(boardData);
		
		getAllListsFromBoardInformation((error2, listsInformation) => {
			if (error2) {
				console.error(error2);
				console.log("Returned to callback function.")
			}
			else {
				console.log("Board ID found and all lists that belong to that board ID are returned successfully inside callback function.");
				console.log(listsInformation);
			}
		}, boardDataArray[0].id);

	}
});