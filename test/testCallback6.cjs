/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/



const fs = require('fs');

const getAllCardsForAllLists = require("../callback6.cjs");

const boardsJSONFilePath = "./boards.json";


fs.readFile(boardsJSONFilePath, (error1, boardData) => {
	if (error1) {
		console.error(error1);
	}
	else {
		let boardDataArray = JSON.parse(boardData);
		let idForThanos = boardDataArray[0].id;
		getAllCardsForAllLists(idForThanos);
	}
});
