/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const fs = require('fs');

const getAllCardsFromListInformation = require("../callback3.cjs");

const listsJSONFilePath = "./lists.json";


fs.readFile(listsJSONFilePath, (error1, listsData) => {
	if (error1) {
		console.error(error1);
	}
	else {
        let listsDataObject = JSON.parse(listsData);
		let listsId = listsDataObject[Object.keys(listsDataObject)[0]][0].id;
	
        
        getAllCardsFromListInformation((error2, listsInformation) => {
			if (error2) {
				console.error(error2);
				console.log("Returned to callback function.")
			}
			else {
				console.log("List ID found and all cards that belong to that list ID are returned successfully inside callback function.");
				console.log(listsInformation);
			}
		}, listsId);

	}
});