module.exports = getAllListsFromBoardInformation;

const fs = require('fs');

function getAllListsFromBoardInformation(callback2, boardId) {
    
  setTimeout(() => {
    
    fs.readFile("./boards.json", (error1, boardInformation) => {
      if (error1) {
        console.log("Enable to read 'boards.json' file");
        callback2(error1, null);
        return
      }
      else {
        // Read Board Data
        let boardJSONData = JSON.parse(boardInformation);
        let boardIdDetails = boardJSONData.filter((boardObject) => {
          if (boardObject.id) {
            if (boardObject.id === boardId) {
              return boardObject;
            }
          }
        });
          
        if (boardIdDetails.length != 0) {

          fs.readFile("./lists.json", (error2, listsInformation) => {
            if (error2) {
              console.log("Enable to read 'lists.json' file");
              callback2(error2, null);
              return
            }
            else {
              let listsJSONData = JSON.parse(listsInformation);
          
              let listsIdDetails = Object.keys(listsJSONData).filter((listsKeys) => {
                if (listsKeys == boardId) {
                  return listsKeys;
                }
              });

              if (listsIdDetails.length != 0) {
                callback2(null, listsJSONData[listsIdDetails]);
                return
              }
              else {
                callback2("List File does not have the given ID.", null);
              }

            };
          });
        }
        else {
          callback2("Board ID does not exist", null);
          return;
        }
        
      
      };
    });

  }, 2000);

}
