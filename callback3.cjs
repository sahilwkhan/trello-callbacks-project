module.exports = getAllCardsFromListInformation;

const fs = require('fs');

function getAllCardsFromListInformation(callback3, listsId) {
    
  setTimeout(() => {
    
    fs.readFile("./cards.json", (error1, cardsInformation) => {
        if (error1) {
            console.log("Enable to read 'cards.json' file");
            callback3(error1, null);
            return
        }
        else {
            // Read Board Data
            let cardsJSONData = JSON.parse(cardsInformation);
        
            for( let cardsKeys in cardsJSONData){
                if (cardsKeys === listsId) {
                    callback3(null, cardsJSONData[cardsKeys]);
                    return
                }
            };
      
        };
    });

  }, 2000);

}
