module.exports = getBoardInformation;

const fs = require('fs');

function getBoardInformation(callback1, boardId) {
    
  setTimeout( ()=>{
    fs.readFile("boards.json", (error1, boardInformation) => {
      if (error1) {
        callback1("Enable to read 'boards.json' file", null);
        return
      }
      else {
        
        let boardJSONData = JSON.parse(boardInformation);
        let boardIdDetails = boardJSONData.filter((boardObject) => {
          if (boardObject.id === boardId) {
              return boardObject;
          }
        });

        if (boardIdDetails.length != 0) {
          callback1(null, boardIdDetails)
        }
        else {
          callback1("Board ID does not exist", null);
          return;
        }

      }  

    });
  }, 2000);
}
