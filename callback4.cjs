module.exports = getAllCardsForMindList;

const fs = require('fs');


const getBoardInformation = require("./callback1.cjs");
const getAllListsFromBoardInformation = require("./callback2.cjs");
const getAllCardsFromListInformation = require("./callback3.cjs");



function getAllCardsForMindList(idForThanos) {
    
       
  setTimeout(() => {

        // Get information from the Thanos boards
        getBoardInformation((error1, boardInformation) => {
            if (error1) {
                console.error(error1);
                console.log("Returned to callback function.")
            }
            else {
                console.log("Board ID found and ID information is returned successfully inside callback function :");
                console.log(boardInformation);

                let listIdForThanos = boardInformation[0].id;

                getAllListsFromBoardInformation((error2, listsInformation) => {
                    if (error2) {
                        console.error(error2);
                        console.log("Returned to callback function.")
                    }
                    else {
                        console.log("Board ID found and all lists that belong to that board ID are returned successfully inside callback function.");
                        console.log(listsInformation);

                        let mindCardId = listsInformation[0].id;

                        getAllCardsFromListInformation((error2, listsInformationForMind) => {
                            if (error2) {
                                console.error(error2);
                                console.log("Returned to callback function.")
                            }
                            else {
                                console.log(`All cards for ${listsInformation[0].name} list :`);
                                console.log(listsInformationForMind);
                            }
                        }, mindCardId);



                    }
                }, listIdForThanos);
                
            }
        }, idForThanos);
    

  }, 2000);

}


