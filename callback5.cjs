module.exports = getAllCardsForMindAndSpaceLists;

const fs = require('fs');


const getBoardInformation = require("./callback1.cjs");
const getAllListsFromBoardInformation = require("./callback2.cjs");
const getAllCardsFromListInformation = require("./callback3.cjs");



function getAllCardsForMindAndSpaceLists(idForThanos) {
    
       
  setTimeout(() => {

        // Get information from the Thanos boards
        getBoardInformation((error1, boardInformation) => {
            if (error1) {
                console.error(error1);
                console.log("Returned to callback function.")
            }
            else {
                console.log("Board ID found and ID information is returned successfully inside callback function :");
                console.log(boardInformation);

                let listIdForThanos = boardInformation[0].id;

                getAllListsFromBoardInformation((error2, listsInformation) => {
                    if (error2) {
                        console.error(error2);
                        console.log("Returned to callback function.")
                    }
                    else {
                        console.log("Board ID found and all lists that belong to that board ID are returned successfully inside callback function.");
                        console.log(listsInformation);

                        let listsIdsForCards;
                        listsIdsForCards = listsInformation.filter((listObject) => {
                            if (listObject.name == "Mind" || listObject.name == "Space") {
                                return listObject;
                            }
                        });

                        // Get cards of all lists ids

                        for (let listObject = 0; listObject < listsIdsForCards.length; listObject++) {
                            getAllCardsFromListInformation((error2, listsInformationForMindAndSpaceCards) => {
                                if (error2) {
                                    console.error(error2);
                                    console.log("Returned to callback function.")
                                }
                                else {
                                    console.log(`Cards for ${listsIdsForCards[listObject].name} are :`);
                                    console.log(listsInformationForMindAndSpaceCards);
                                }
                            }, listsIdsForCards[listObject].id);
                            
                        };

                    }
                }, listIdForThanos);
                
            }
        }, idForThanos);
    

  }, 2000);

}


